package com.example.TiendaVirtual;

import com.example.TiendaVirtual.modelos.Producto_db;
import com.example.TiendaVirtual.modelos.Usuario;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import com.google.gson.Gson;
import java.util.Collections;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/*import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;*/

@SpringBootApplication // Donde se encuentre este arroba, es el Main.
@RestController // Indica que la clase será un API REST.
public class TiendaVirtualApplication {
    
    
    

    @Autowired     // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Producto_db p; // Instancias - Objeto 
    @Autowired
    Usuario u; // Instancia
    // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.
    //Objeto=la clase, llamar la clase
    //get maping envia los paramatros al navegador

    public static void main(String[] args) {
		SpringApplication.run(TiendaVirtualApplication.class, args);
                SpringApplication app = new SpringApplication(
                TiendaVirtualApplication.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", "8084"));
        app.run(args);
    }

    //EJEMPLO
    /*@GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "20") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);
    }*/
    
    @GetMapping("/usuario")
    public String consultarUsuarioPorID(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {
       u.setId_usuario(id);
        if (u.consultar()) { // True
            String res = new Gson().toJson(u);
            u.setId_usuario(0); // Si hay más registros SOLO traigase el primero que encuentre
            u.setNombre("");
            u.setApellido("");
            u.setCorreo("");
            u.setContrasena("");
            u.setFecha_creacion("");
            return res;
        } else { // False
            
            return new Gson().toJson(u);
        }
    }

    /*@GetMapping("/producto_db")
    public String consultarUnProductoPorID(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {
        p.setId_producto(id);
        if (p.consultarProducto()) {
            return new Gson().toJson(p);
        } else {
            return new Gson().toJson(p);
        }
    }*/
    
    
    /*@GetMapping("/productos")
    public String consultarProductosPorUsuario(@RequestParam(value = "id", defaultValue = "1") int id) throws ClassNotFoundException, SQLException {       
        List<Producto_db>productos = p.consultarTodo(id);
        if(productos.size()> 0){
            return new Gson().toJson(productos);
            //return "{\"id\":\""+c.getId()+"\",\"numcuenta\":\""+c.getNumero_cuenta()+"\",\"saldo\":\""+c.getSaldo()+"\",\"cvv\":\""+c.getCvv()+"\"}";
        } else {
            return new Gson().toJson(productos);
            //return "{\"id\":\""+cedula+"\",\"numcuenta\":\""+"La cédula no tiene asociada una cuenta."+"\"}";
        }   
    }*/
    
    
    // GET traer - POST mandarlo, crear - PUT actualizarlo - DELETE : eliminar
    @PostMapping(path = "/usuario", // le enviamos datos al servidor
        consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
        produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definición   
    // {nombre = Andres, edad = 15}
    // f=json
    public String actualizarUsuario(@RequestBody String usuario) throws ClassNotFoundException, SQLException{
                
        Usuario f = new Gson().fromJson(usuario,Usuario.class); // recibimos el json y lo devolvemos un objeto estudiante
        //u.setId_usuario(f.getId_usuario()); // obtengo el id de f y se lo ingreso e
        u.setCorreo(f.getCorreo());
        u.setContrasena(f.getContrasena());
        u.setNombre(f.getNombre());
        u.setApellido(f.getApellido());
        u.actualizar();
        return new Gson().toJson(u); // cliente le envia json al servidor. fpr de comunicarse
        
    }
    
    /*@DeleteMapping("/eliminarproducto/{id}")
    public String borrarProducto(@PathVariable("id") int id) throws SQLException, ClassNotFoundException{
        p.setId_producto(id);
        p.borrarProducto();
        return "Los datos del id indicado han sido eliminados";
    }*/
    
    @DeleteMapping("/eliminarusuario/{id}")
    public String borrarProducto(@PathVariable("id") int id) throws SQLException, ClassNotFoundException{
        u.setId_usuario(id);
        u.borrar();
        return "Los datos del id indicado han sido eliminados";
    }
    
    
    
    @PostMapping(path = "/guardarusuario", // le enviamos datos al servidor
        consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
        produces = MediaType.APPLICATION_JSON_VALUE)
    public String insertarUsuario(@RequestBody String usuario) throws ClassNotFoundException, SQLException {
        Usuario es = new Gson().fromJson(usuario, Usuario.class);
        u.setCorreo(es.getCorreo());
        u.setContrasena(es.getContrasena());
        u.setNombre(es.getNombre());
        u.setApellido(es.getApellido());
        u.guardarUsuario();
        return new Gson().toJson(u);
    }   
    
}

