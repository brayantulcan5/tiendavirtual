
package com.example.TiendaVirtual.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component ("producto_db")
public class Producto_db {
   
    @Autowired
    transient JdbcTemplate jdbcTemplate;

    private int id_producto;
    private String nombre_producto;
    private double precio_compra;
    private String categoria;
    private int cantidad;
    private Usuario id_usuario; //llave foránea

    // constructor
    public Producto_db(int id_producto, String nombre_producto, double precio_compra, String categoria, int cantidad) {
        super(); //Herda de la Clase Principal
        this.id_producto = id_producto;
        this.nombre_producto = nombre_producto;
        this.precio_compra = precio_compra;
        this.categoria = categoria;
        this.cantidad = cantidad;
    }

    public Producto_db() {
    }

    //Mètodos Get & Set
    
    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public double getPrecio_compra() {
        return precio_compra;
    }

    public void setPrecio_compra(double precio_compra) {
        this.precio_compra = precio_compra;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Usuario getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Usuario id_usuario) {
        this.id_usuario = id_usuario;
    }

    @Override
    public String toString() {
        return "Producto_db{" + "id_producto=" + id_producto + ", nombre_producto=" + nombre_producto + ", precio_compra=" + precio_compra + ", categoria=" + categoria + ", cantidad=" + cantidad + ", id_usuario=" + id_usuario + '}';
    }

    
   
    // CRUD
    // Guardar
    public String guardarProducto() throws ClassNotFoundException, SQLException{
        String  sql = "INSERT INTO producto_db(id_producto, nombre_producto, precio_compra, categoria, cantidad) VALUES(?,?,?,?,?)";
        return sql;
    }
    
    // Consultar
    public boolean consultarProducto() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        String  sql = "SELECT id_producto,nombre_producto,precio_compra,categoria,cantidad,id_usuario FROM producto_db WHERE id_usuario = ?";
        List<Producto_db> productos = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Producto_db(
                        rs.getInt("id_producto"), // => 123
                        rs.getString("nombre_producto"), // => 
                        rs.getDouble("precio_compra"),
                        rs.getString("categoria"),
                        rs.getInt("cantidad")
                ), new Object[]{this.getId_usuario()});
        if (productos != null &&  productos.size() > 0 ) {
            this.setId_producto(productos.get(0).getId_producto());
            this.setNombre_producto(productos.get(0).getNombre_producto());
            this.setPrecio_compra(productos.get(0).getPrecio_compra());
            this.setCategoria(productos.get(0).getCategoria());
            this.setCantidad(productos.get(0).getCantidad());
            return true;
        }
        else {
            return false;
        }
    }
    
    public List<Producto_db> consultarTodo(int id_usuario) throws ClassNotFoundException, SQLException{
        //CRUD -R 
        String  sql = "SELECT id_producto,nombre_producto,precio_compra,categoria,cantidad,id_usuario FROM producto_db WHERE id_usuario = ?";
        List<Producto_db> productos = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Producto_db(
                        rs.getInt("id_producto"), // => 123
                        rs.getString("nombre_producto"), // => 
                        rs.getDouble("precio_compra"),
                        rs.getString("categoria"),
                        rs.getInt("cantidad")
                ), new Object[]{this.getId_usuario()});
        
        return productos;
    }

    // Actualizar 
    public String actualizarProducto() throws ClassNotFoundException, SQLException{
        // CRUD -U
        String sql = "UPDATE producto_db SET nombre_producto = ?,precio_compra = ?, categoria = ?, cantidad = ? WHERE id_producto = ?";
        return sql;
    }
    
    // Borrar
    public boolean borrarProducto() throws SQLException, ClassNotFoundException{
        //CRUD -D
        String sql = "DELETE FROM producto_db WHERE id_producto = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_producto());
        ps.execute();
        ps.close();
        
        return true;
   
    }
    
}

