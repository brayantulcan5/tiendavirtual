
package com.example.TiendaVirtual.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component ("usuario")

public class Usuario {
    @Autowired
    transient JdbcTemplate jdbcTemplate; //paquete que nos permite ejecutar las consulta sql
    //Puede que no siempre se ejecute la consulta
   
    //Atributos
       private int id_usuario;
       private String nombre;
       private String apellido;
       private String correo;
       private String contrasena;
       private String fecha_creacion;
    
    // Constructor
    public Usuario(int id_usuario, String nombre, String apellido, String correo, String contrasena, String fecha_creacion) {
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.contrasena = contrasena;
        this.fecha_creacion = fecha_creacion;
    }

    public Usuario() {
    }
    
    // Metodos Get & Set

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }    

    @Override
    public String toString() {
        return "Usuario{" + "id_usuario=" + id_usuario + ", nombre=" + nombre + ", apellido=" + apellido + ", correo=" + correo + ", contrasena=" + contrasena + ", fecha_creacion=" + fecha_creacion + '}';
    }
           
    // CRUD    
    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        String  sql = "SELECT id_usuario, nombre, apellido, correo, contrasena, fecha_creacion FROM usuario WHERE id_usuario = ?";
        List<Usuario> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuario(
                         rs.getInt("id_usuario"),  // => 123
                         rs.getString("nombre"), // => 
                         rs.getString("apellido"), 
                         rs.getString("correo"),
                         rs.getString("contrasena"),
                         rs.getString("fecha_creacion")
                 ), new Object[]{this.getId_usuario()});
        if (usuarios != null &&  usuarios.size() > 0 ) {
            this.setId_usuario(usuarios.get(0).getId_usuario());
            this.setNombre(usuarios.get(0).getNombre());
            this.setApellido(usuarios.get(0).getApellido());
            this.setCorreo(usuarios.get(0).getCorreo());
            this.setContrasena(usuarios.get(0).getContrasena());
            this.setFecha_creacion(usuarios.get(0).getFecha_creacion());

            
            return true;
        }
        else {
            return false;
        }
    } 
    
    //CRUD -R 
    /*public List<Usuario> consultarTodo(int Tipos_usuario_id_tipo_usuario) throws ClassNotFoundException, SQLException{
        String  sql = "SELECT id_usuario, nombre, apellido, correo, contrasena, fecha_creacion FROM usuario WHERE id_usuario = ?";
        List<Usuario> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuario(
                         rs.getInt("id_usuario"),  // => 123
                         rs.getString("nombre"), // => 
                         rs.getString("apellido"), 
                         rs.getString("correo"),
                         rs.getString("contrasena"),
                         rs.getString("fecha_creacion")
               ), new Object[]{this.getId_usuario()});    
            
        return usuarios;

    }*/
    
    // Actualizar 
    public String actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        String sql = "UPDATE usuario SET nombre = ?, apellido = ?, correo = ?, contrasena = ? WHERE id_usuario = ?";
       Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre());
        ps.setString(2, this.getApellido());
        ps.setString(3, this.getCorreo());
        ps.setString(4, this.getContrasena());
        ps.executeUpdate();
        ps.close();
        return sql;
    }
    
    // Borrar
    
    public boolean borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D
        String sql = "DELETE FROM usuario WHERE id_usuario = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_usuario());
        ps.execute();
        ps.close();
        
        return true;
   
    }
    
    
    // Guardar
    public int guardarUsuario() throws ClassNotFoundException, SQLException{
        int last_inserted_id = -1;
        String  sql = "INSERT INTO usuario(id_usuario, nombre, apellido, correo, contrasena, fecha_creacion) VALUES(?,?,?,?,?,?)";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre());
        ps.setString(2, this.getApellido());
        ps.setString(3, this.getCorreo());
        ps.setString(4, this.getContrasena());
        ps.setString(4, this.getFecha_creacion());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys(); // llave generada del PS -- insertar con ID
        if (rs.next()) {
            last_inserted_id = rs.getInt(1);
        }
        ps.close();
        return last_inserted_id;
    }
    
 }
