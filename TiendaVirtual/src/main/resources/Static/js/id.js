// JavaScript source code
angular.module('tiendavirtual',[])
.controller('filaUsuario',function($scope,$http){
    
    $scope.nombre = "";
    $scope.apellido = "";
    $scope.correo = "";
    $scope.contrasena = "";
    
    // accion del boton consultar
    $scope.consultar = function(){
        if($scope.id === undefined || $scope.id === null){
            $scope.id = 0;
        }
        
        $http.get("/tiendavirtual/usuario?id="+$scope.id).then(function(data){
            console.log(data.data);
            $scope.nombre = data.data.nombre;
            $scope.apellido = data.data.apellido;
            $scope.correo = data.data.correo;
            $scope.contrasena = data.data.contrasena;
        },function(){
             //error
            $scope.nombre = "";
            $scope.apellido = "";
            $scope.correo = "";
            $scope.contrasena = "";
            $scope.filas = []; // guarda
        });
        
        $http.get("/tiendavirtual/usuario?id="+$scope.id).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
    
    //acción del botón actualizar
    $scope.actualizar = function(){
        if($scope.id === undefined || $scope.id === null){
            $scope.id = 0;
        }
        data = {
            "id": $scope.id,
            "nombre":$scope.nombre,
            "apellido": $scope.apellido,
            "correo": $scope.correo,
            "contrasena": $scope.contrasena
        };
        $http.post('/tiendavirtual/usuario', data).then(function(data){
            //success
            $scope.nombre = data.data.nombre;
            $scope.apellido = data.data.apellido;
            $scope.correo = data.data.correo;
            $scope.contrasena = data.data.contrasena;
        },function(){
            //error
            $scope.nombre = "";
            $scope.apellido = "";
            $scope.correo = "";
            $scope.contrasena = "";
            $scope.filas = [];
        });
        
    };
    
    $scope.guardar = function(){
        data = {
            "id": $scope.id,
            "nombre":$scope.nombre,
            "apellido": $scope.apellido,
            "correo": $scope.correo,
            "contrasena": $scope.contrasena,
            "fecha_creacion": $scope.fecha_creacion
        };
        $http.post('/tiendavirtual/usuario', data).then(function(data){
            //success
            $scope.nombre = data.data.nombre;
            $scope.apellido = data.data.apellido;
            $scope.correo = data.data.correo;
            $scope.contrasena = data.data.contrasena;
            $scope.fecha_creacion = data.data.fecha_creacion;
        },function(){
            //error
            $scope.nombre = "";
            $scope.apellido = "";
            $scope.correo = "";
            $scope.contrasena = "";
            $scope.fecha_creacion = "";
            $scope.filas = [];
        });
        
    };
         
    // acción del botón borrar
    $scope.borrar = function(){
        if($scope.id === undefined || $scope.id === null){
            $scope.id = 0;
        }
        data = {
            "id": $scope.id
        };
        $http.delete('/tiendavirtual/eliminarusuario/'+$scope.id, data).then(function(data){
            alert("El Producto ha sido eliminado");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };
});
