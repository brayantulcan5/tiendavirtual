-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema tiendavirtual6
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tiendavirtual6
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tiendavirtual6` DEFAULT CHARACTER SET swe7 ;
USE `tiendavirtual6` ;

-- -----------------------------------------------------
-- Table `tiendavirtual6`.`inventario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual6`.`inventario` (
  `id_inventario` INT NOT NULL AUTO_INCREMENT,
  `usuario_id_usuario` INT NOT NULL,
  `nombre_producto` VARCHAR(45) NULL DEFAULT NULL,
  `cantidad_final` INT NULL DEFAULT NULL,
  `tipo_operacion` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_inventario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = swe7;


-- -----------------------------------------------------
-- Table `tiendavirtual6`.`producto_bd`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual6`.`producto_bd` (
  `id_producto` INT NOT NULL AUTO_INCREMENT,
  `nombre_producto` VARCHAR(45) NULL DEFAULT NULL,
  `precio_compra` DOUBLE NULL DEFAULT NULL,
  `categoria` VARCHAR(45) NULL DEFAULT NULL,
  `cantidad` INT NULL DEFAULT NULL,
  `inventario_id_inventario` INT NOT NULL,
  PRIMARY KEY (`id_producto`, `inventario_id_inventario`),
  INDEX `fk_producto_bd_inventario_idx` (`inventario_id_inventario` ASC) VISIBLE,
  CONSTRAINT `fk_producto_bd_inventario`
    FOREIGN KEY (`inventario_id_inventario`)
    REFERENCES `tiendavirtual6`.`inventario` (`id_inventario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = swe7;


-- -----------------------------------------------------
-- Table `tiendavirtual6`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual6`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `apellido` VARCHAR(45) NULL DEFAULT NULL,
  `correo` VARCHAR(45) NULL DEFAULT NULL,
  `contrasena` VARCHAR(45) NULL DEFAULT NULL,
  `fecha_creacion` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = swe7;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
